package com.simsoft.api.samplejavadocker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.simsoft.api.samplejavadocker.repository")
public class SampleJavaDockerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SampleJavaDockerApplication.class, args);
	}

}
