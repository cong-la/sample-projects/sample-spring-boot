package com.simsoft.api.samplejavadocker.controller;

import com.simsoft.api.samplejavadocker.model.SchoolEvent;
import com.simsoft.api.samplejavadocker.repository.SchoolEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/sample")
public class SampleController {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @GetMapping()
    public String getSampleMessage() {
        return "This is sample controller";
    }

    @GetMapping(path = "/event")
    public ArrayList<SchoolEvent> getSchoolEvents() {
        return this.schoolEventRepository.findAll();
    }

    @PostMapping(path = "/event")
    public SchoolEvent addSchoolEvent(@RequestBody SchoolEvent schoolEvent) {
        return this.schoolEventRepository.save(schoolEvent);
    }
}
