package com.simsoft.api.samplejavadocker.repository;

import com.simsoft.api.samplejavadocker.model.SchoolEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

public interface SchoolEventRepository extends JpaRepository<SchoolEvent, Integer> {

    public ArrayList<SchoolEvent> findAll();

    public SchoolEvent save(SchoolEvent schoolEvent);

}
