package com.simsoft.api.samplejavadocker.model;

import lombok.*;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "school_event")
public class SchoolEvent {

    @Id
    @Column(name = "event_id")
    private int eventId;

    @Column(name = "name")
    private String name;

}
