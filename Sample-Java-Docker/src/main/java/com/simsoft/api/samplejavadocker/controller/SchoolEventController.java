package com.simsoft.api.samplejavadocker.controller;

import com.simsoft.api.samplejavadocker.model.SchoolEvent;
import com.simsoft.api.samplejavadocker.repository.SchoolEventRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping("/event")
public class SchoolEventController {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @GetMapping()
    public ArrayList<SchoolEvent> getSchoolEvents() {
        return this.schoolEventRepository.findAll();
    }

    @PostMapping()
    public SchoolEvent addSchoolEvent(@RequestBody SchoolEvent schoolEvent) {
        return this.schoolEventRepository.save(schoolEvent);
    }
}
